# LMB ejercicio datos


### Un analisís del de las estadisticas de bateo de la Liga Mexicana de Beisbol de las temporadas 2006 a 2009.





## Relacionado

[Estadisticas de la temporada](https://www.fangraphs.com/leaders/minor-league?pos=all&stats=bat&lg=31&qual=y&type=0&season=2019&sort=23,1)






## Tech Stack

**Software:** Python, PySpark, Matplotlib, Plotly



## License

[MITLicense](https://choosealicense.com/licenses/mit/) 

Copyright (c) 2023 Garcia Dojaquez Joel Nicolás

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.


## 🔗 Links
[![linkedin](https://img.shields.io/badge/linkedin-0A66C2?style=for-the-badge&logo=linkedin&logoColor=white)](https://www.linkedin.com/in/joel-garc%C3%ADa-8b496119b/)

